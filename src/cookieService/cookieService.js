import {listStaff} from "../list";

export const getListStaff = () => {
    const list = getCookie("listStaff");
    try {
        return JSON.parse(list)  
    } catch (e) {
        console.error ('error')
    }
    
}

const getCookie = (name) => {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : setCookie(name);
}

const setCookie = (name) => {
    document.cookie = name + '=' + JSON.stringify(listStaff)
    return JSON.stringify(listStaff)
}