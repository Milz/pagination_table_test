import {applyMiddleware, combineReducers, createStore} from "redux";
import listReducer from "./listReducer";
import thunk from 'redux-thunk';

let reducers = combineReducers({
    listReducer
})

let store = createStore(reducers, applyMiddleware(thunk));

export default store