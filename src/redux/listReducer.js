import {getListStaff} from "../cookieService/cookieService";

const SET_STAFF = 'SET-STAFF';
const SORT_STAFF = 'SORT-STAFF';
const SET_CURRENT_PAGE = 'SET-CURRENT-PAGE';

let initialState = {
    staff: [],
    currentPage: 1,
    pageSize: 3
}

const listReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_STAFF:
            return {
                ...state,
                staff: action.staff.members
            }
        case SET_CURRENT_PAGE:
            return {
                ...state,
                currentPage: action.page
            }
        case SORT_STAFF:
            let name = action.key;
            let arr = [...state.staff];
            arr.sort((a, b) => {
                if (name === 'id') {
                    a[name] = +a[name];
                    b[name] = +b[name];
                }
                if (a[name] < b[name]) {
                    return action.directionUp ? -1 : 1;
                }
                if (a[name] > b[name]) {
                    return action.directionUp ? 1 : -1;
                }
                return 0;
            })

            return {
                ...state,
                staff: arr
            }
        default:
            return state;
    }
}

const setListStaff = (staff) => ({type: SET_STAFF, staff});
export const sortStaff = (key, directionUp) => ({type: SORT_STAFF, key, directionUp})
export const onPageChange = (page) => ({type: SET_CURRENT_PAGE, page})
export const initializeTable = () => (distpach) => {
    let data = getListStaff()
    distpach(setListStaff(data))
};

export default listReducer