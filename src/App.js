import React, {Component} from 'react';
import './App.css';
import {connect} from "react-redux";
import {initializeTable} from "./redux/listReducer";
import Table from "./Components/Table/Table";
import Paginator from "./Components/Paginator/Paginator";
import {Route} from "react-router-dom";


const App = (props) => {
    props.initializeTable();
    return (
        <div className='container'>
            <div className='row'>
                <div>
                    <Paginator/>
                    <Route path='/:page?'
                           render={() => <Table />}
                           />
                </div>
            </div>
        </ div>
);
}

export default connect(null, {initializeTable})(App);
