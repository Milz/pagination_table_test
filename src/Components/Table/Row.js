import React from 'react';

const Row = ({id, name, position}) => {
    return <tr>
        <td>{id}</td>
        <td>{name}</td>
        <td>{position}</td>
    </tr>
}

export default Row