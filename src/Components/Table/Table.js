import React, {useState} from 'react';
import {onPageChange, sortStaff} from "../../redux/listReducer";
import {connect} from "react-redux";
import Row from "./Row";
import {withRouter} from "react-router-dom";
import {compose} from "redux";

const Table = (props) => {
    const [directionId, setDirectionId] = useState(true);
    const [directionName, setDirectionName] = useState(true);
    const [directionPosition, setDirectionPosition] = useState(true);

    const sortStaff = (name) => {
        switch (name) {
            case 'id':
                props.sortStaff(name, directionId);
                setDirectionId(!directionId);
                break;
            case 'name':
                props.sortStaff(name, directionName);
                setDirectionName(!directionName);
                break;
            case 'position':
                props.sortStaff(name, directionPosition);
                setDirectionPosition(!directionPosition);
                break;
            default:
                break;
        }
    }

    if (props.currentPage !== props.match.params.page) {
        if(props.match.params.page) {
            props.onPageChange(props.match.params.page)
        } else {
            props.onPageChange(props.currentPage)
        }

    }
    let members = props.staff;
    let table = [];
    let start = props.currentPage * props.pageSize - props.pageSize;
    let finish = (start + props.pageSize) < members.length ?
        start + props.pageSize :
        members.length;
    if (members) {
        for (let i = start; i < finish; i++) {
            table.push(<Row id={members[i].id}
                            key={members[i].id}
                            name={members[i].name}
                            position={members[i].position}/>)
        }
    }
    return <div>
        <table className="table">
            <thead>
            <tr>
                <td>
                    <button className='btn' onClick={() => {
                        sortStaff('id', directionId)
                    }}>
                        №
                    </button>
                </td>
                <td>
                    <button className='btn' onClick={() => {
                        sortStaff('name')
                    }}>
                        name
                    </button>
                </td>
                <td>
                    <button className='btn' onClick={() => {
                        sortStaff('position')
                    }}>
                        position
                    </button>
                </td>
            </tr>
            </thead>
            <tbody>
            {table}
            </tbody>
        </table>
    </div>
}

let mapStateToProps = (state) => {
    return {
        staff: state.listReducer.staff,
        currentPage: state.listReducer.currentPage,
        pageSize: state.listReducer.pageSize
    }
}

export default compose(
    withRouter,
    connect(mapStateToProps, {sortStaff, onPageChange})
)(Table)