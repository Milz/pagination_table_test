import React from 'react';
import {connect} from "react-redux";
import {onPageChange} from "../../redux/listReducer";
import {NavLink} from "react-router-dom";

const Paginator = ({staff, currentPage, pageSize, onPageChange}) => {
    let pageCount = Math.ceil(staff.length / pageSize);
    let pages = [];
    for (let i = 1; i <= pageCount; i++) {
        pages.push(i)
    }
    return <ul className="pagination">
        {pages.map(p => <li key={p} className="page-item">
            <NavLink to={'/' + p}
                     className='page-link'
                     onClick={() => {
                         onPageChange(p)
                     }}>{p}</NavLink>

            </li>)}
    </ul>
}

let mapStateToProps = (state) => {
    return {
        staff: state.listReducer.staff,
        currentPage: state.listReducer.currentPage,
        pageSize: state.listReducer.pageSize
    }
}

export default connect(mapStateToProps, {onPageChange})(Paginator)